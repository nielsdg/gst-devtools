---
title: Configuration
short-description: GstValidate configuration
...

# GstValidate Configuration

GstValidate comes with some possible configuration files
to setup its plugins (and potentially core behaviour).
